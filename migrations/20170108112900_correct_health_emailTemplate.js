
exports.up = function(knex) {
  const emailTemplate = `
    <p>
      Dear {{CongressPerson}},
    </p>

    <p>
      I am a citizen concerned about the future of the healthcare system in our Country.  With the new
      administration, we are told many changes are on the way, including the possibility of a repeal of
      Affordable Care Act. I hope you do not vote for the law's repeal, and instead work to fix
      its problems. If, however, you do feel the law must be repealed, I hope that you will ensure that
      there is a replacement ready that will guarantee the following:
    </p>
    
    <ol>
      <li>Access to affordable healthcare for all, including those with pre-existing conditions</li>
      <li>Equal treatment of women in the system</li>
      <li>A plan to address sharp increases in insurance premiums</li>
      <li>Access to affordable prescription drugs</li>
      <li>That the 20+ million people who have benefited from healthcare reform keep their coverage</li>
    </ol>

    <p>
      I hope you will work with your colleagues on both sides of the aisle to resolve this highly
      politicized issue once and for all.
    </p>

    <p>
      Thank you for your time.
    </p>

    <p>
      Sincerely,<br />
      {{Signature}}      
    </p>
  `;    
  
  return knex("issues")
    .where({ issueSlug: "health" })
    .update({emailTemplate: emailTemplate})
};

exports.down = function(knex) {
const emailTemplate = `
    <p>
      Dear {{CongressPerson}},
    </p>

    <p>
      I am a citizen concerned about the future of the healthcare system in our country.  With the new
      administration, we are told many changes are on the way, including the possibility of a repeal of
      the Affordable Care Act. I hope you do not vote for the law's repeal, and instead work to improve fix
      its problems. If, however, you do feel the law must be repealed, I hope that you will ensure that
      there is a replacement ready that will ensure the following:
    </p>
    
    <ol>
      <li>Access to affordable healthcare for all, including those with pre-existing conditions</li>
      <li>Equal treatment of women in the system</li>
      <li>A plan to address sharp increases in insurance premiums</li>
      <li>Access to affordable prescription drugs</li>
      <li>That the 20+ million people who have benefited from healthcare reform keep their coverage</li>
    </ol>

    <p>
      I hope you will work with your colleagues on both sides of the aisle to resolve this highly
      politicized issue once and for all.
    </p>

    <p>
      Thank you for your time.
    </p>

    <p>
      Sincerely,<br />
      {{Signature}}      
    </p>
  `;
  
  return knex("issues")
    .where({ issueSlug: "health" })
    .update({emailTemplate: emailTemplate})
};
