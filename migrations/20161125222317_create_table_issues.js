
exports.up = function(knex) {
    return knex.schema.createTable('issues', function (t) {
      t.increments('issueId').primary();
      t.string('langLocale', 5);
      t.string('issueSlug', 25);
      t.string('subtext', 255);
      t.text('description');
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('issues'); 
};
