
exports.up = function(knex) {
  return knex.schema.table('issues', function (t) {
    t.text('emailTemplate');
  });
};

exports.down = function(knex) {
  return knex.schema.table('issues', function (t) {
    t.dropColumn('emailTemplate');
  });
};
