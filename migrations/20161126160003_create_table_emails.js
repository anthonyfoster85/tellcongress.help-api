
exports.up = function(knex) {
    return knex.schema.createTable('emails', function (t) {
        t.increments('emailId').primary();
        t.integer('issueId').unsigned().references('issueId').inTable('issues');
        t.string('to', 255);
        t.timestamp('sentDate');
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('emails'); 
};
