
exports.up = function(knex, Promise) {
  if (knex('issues').where({issueSlug: 'health'}).length > 0) {
    return Promise.resolve();
  }
  
  const issue = {
    issueSlug: 'health',
    langLocale: 'en-us',
    subtext: 'to make common sense decisions about healthcare.',
    description: `
      <p>
          With the new congress and administration coming to power in early 2017, we will likely see changes to, if not the full repeal of,
          the Affordable Care Act (also known as Obamacare).  While the law has been contreversial from the start, most agree on the principles
          with which it set out to address:
      </p>

      <ul>
          <li>Access to affordable health care for all</li>
          <li>Access to health insruance to those with existing conditions which would normally have made health insurance unaffordable</li>
          <li>Equal treatment of women in the health care system</li>
      </ul>

      <p>
          20+ million people are now insured thanks to this law, but recently, insurance premiums have been increasing sharply nation wide, 
          putting a huge burden on those that are already in a tough financial spot. Let's tell Congress to address increasing premiums, and 
          other health care concerns of the nation.
      </p>`
  };

  return knex('issues').insert(issue);
};

exports.down = function(knex) {
  return knex('issues').where({issueSlug: 'health'}).delete();
};
