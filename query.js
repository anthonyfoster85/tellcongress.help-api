var config = require('./knexfile.js');
var db = require('knex')(config.development);

var query = process.argv.length >= 3 ? process.argv[2] : null;
if (!query) {
    console.log('No query provided');
    process.exit(1);
}

db.raw(query).then(function(result) {
    console.log(result);
    process.exit();
});
