import knex from 'knex';
import config from '../knexfile';

export default callback => {
	let db = knex(config[process.env.NODE_ENV]);

	db.migrate.latest().then(() => {
		callback(db);
	});
}
