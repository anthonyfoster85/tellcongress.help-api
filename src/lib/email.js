function escapeHtml(unsafe) {
    return unsafe
         .replace(/&/g, "&amp;")
         .replace(/</g, "&lt;")
         .replace(/>/g, "&gt;")
         .replace(/"/g, "&quot;")
         .replace(/'/g, "&#039;");
 }

export function fillTemplate (template, gender, legislator, signature) {
  let validGender = gender ? gender.toUpperCase() : '';
  let salutation = validGender === 'M' ? 'Mr.' :
    validGender === 'F' ? 'Ms.' : null;
  let congressPerson = salutation ? `${salutation} ${legislator}` : legislator;

  let result = template;
  if (congressPerson) {
    result = result.replace('{{CongressPerson}}', escapeHtml(congressPerson));
  }

  if (signature || signature === '') {
    result = result.replace('{{Signature}}', escapeHtml(signature));
  }

  return result
}
