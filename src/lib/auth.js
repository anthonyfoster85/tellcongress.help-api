import request from 'request';
import recaptcha from '../../deploy/recaptcha.json'

export function validateRecaptcha(token) {
  return new Promise((resolve, reject) => {
    if (!token) {
      reject();
    }

    let form = {
      secret: recaptcha.secret,
      response: token
    };

    request.post({ url: 'https://www.google.com/recaptcha/api/siteverify', form }, (err, httpResponse, body) => {
      let response = JSON.parse(body);
      if (err || !response.success) {
        reject();
      }
      resolve();
    });
  });
}