import request from 'request';
import { Router } from 'express';
import testData from '../../deploy/testData.json';

export default () => {
  let route = Router();

  const API_KEY = '$API_KEY';

  function parseLegislatorResult(results) {
    return results.map((d) => ({
      first_name: d.first_name, 
      last_name: d.last_name, 
      chamber: d.chamber, 
      email: d.oc_email,
      gender: d.gender
    }));
  }

  route.get('/legislators/:zip', (apiRequest, apiResponse) => {
    let zip = apiRequest.params.zip;
    if (zip === testData.testZip) {
      let results = parseLegislatorResult(testData.testLegislators);
      apiResponse.json(results);
      return;
    }

    let congressUrl = `https://congress.api.sunlightfoundation.com/legislators/locate?apikey=${API_KEY}&zip=${zip}`;
    request(congressUrl, (error, response, body) => {
      if (!error && response.statusCode == 200) {
        let data = JSON.parse(body);

        let results = parseLegislatorResult(data.results);
        apiResponse.json(results);
      }
    });
  });

  route.get('/legislators/:latitude/:longitude', (apiRequest, apiResponse) => {
    let congressUrl = `https://congress.api.sunlightfoundation.com/legislators/locate?apikey=${API_KEY}&latitude=${apiRequest.params.latitude}&longitude=${apiRequest.params.longitude}`;
    request(congressUrl, (error, response, body) => {
      if (!error && response.statusCode == 200) {
        let data = JSON.parse(body);
        apiResponse.json(parseLegislatorResult(data.results));
      }
    });
  });

  return route;
}
