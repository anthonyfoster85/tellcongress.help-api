import { Router } from 'express';
import issueModel from '../models/issues'
import { fillTemplate } from '../lib/email'

export default (db) => {
  let route = Router();

  route.get('/:slug', (apiRequest, apiResponse) => {
    issueModel(db).loadIssue(apiRequest.params.slug).then((issue) => {
      return apiResponse.json(issue);
    }, () => {
      apiResponse.status(404).json({ error: 'Not Found' });
    });    
  });

  route.get('/:slug/template', (apiRequest, apiResponse) => {
    issueModel(db).loadEmailTemplate(apiRequest.params.slug).then((issueTmeplate) => {
      let { gender, legislator, signature } = apiRequest.query;
      gender = gender ? gender.toUpperCase() : null;

      let template = fillTemplate(issueTmeplate, gender, legislator, signature);
      apiResponse.json({ template });
    }, () => {
      apiResponse.status(404).json({ error: 'Not Found' });
    });
  });

  return route;
}
