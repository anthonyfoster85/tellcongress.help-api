import { Router } from 'express';
import test from './test';
import congress from './congress';
import gmail from './gmail';
import issues from './issues';

export default (db) => {
	let api = Router();

	// mount the test route
	api.use('/v1/test', test());
	api.use('/v1/congress', congress());
	api.use('/v1/gmail', gmail(db));
	api.use('/v1/issues', issues(db));

	return api;
}
