import { Router } from 'express';

export default () => {
  let route = Router();

  route.get('/', (request, response) => {
    response.json(new Date().toUTCString());
  });

  return route;
}
