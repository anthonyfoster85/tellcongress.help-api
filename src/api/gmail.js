import Credentials from '../../deploy/googleCredentials.json';
import { Router } from 'express';
import google from 'googleapis';
import moment from 'moment';
import { fillTemplate } from '../lib/email';
import { validateRecaptcha } from '../lib/auth';
import issuesModel from '../models/issues';

const OAuth2 = google.auth.OAuth2;
const Gmail = google.gmail('v1');

export default (db) => {
	let route = Router();

	route.get('/redirectUrl', (apiRequest, apiResponse) => {
		let authHeader = apiRequest.get('authorization');
		// let remoteip = apiRequest.headers['x-forwarded-for'] || apiRequest.connection.remoteAddress;
		validateRecaptcha(authHeader).then(() => {
			let state = JSON.parse(apiRequest.query.state);
			if (!state.from) {
				apiResponse.status(400).json({
					error: "validation",
					field: "state.from",
					code: "Required"
				});
				return;
			}

			let oauth2Client = new OAuth2(
				Credentials.client_id,
				Credentials.client_secret,
				Credentials.redirect_url
			);
		
			var url = oauth2Client.generateAuthUrl({
				state: JSON.stringify(state),
				access_type: 'online',
				scope: ['https://www.googleapis.com/auth/gmail.send']
			});

			apiResponse.json(url);
		}, () => {
			apiResponse.status(401).json({
				error: "unauthorized"
			});
		});
	});

	route.get('/oauth2callback', (apiRequest, apiResponse) => {
		let state = apiRequest.query.state ? JSON.parse(apiRequest.query.state) : null;
		let code = apiRequest.query.code;

		let oauth2Client = new OAuth2(
			Credentials.client_id,
			Credentials.client_secret,
			Credentials.redirect_url
		);

		oauth2Client.getToken(code, function (error, tokens) {
			if (error) {
				console.log('Error getting token.');
				return;
			}

			oauth2Client.setCredentials(tokens);
			
			return issuesModel(db).loadEmailTemplate(state.issue)
				.then((template) => {		
					Promise.all(state.to.map((to) => {
						if (!to.email) {
							return Promise.resolve();
						}
						let filledTemplate = fillTemplate(template, to.gender, to.name, state.from);
						return sendEmail(oauth2Client, to, state.from, filledTemplate).then(() => {
							return db('emails').insert({ 
								issueId: state.issueId, 
								to: to.email,
								sentDate: moment(new Date().getTime()).format("YYYY-MM-DD HH:mm:ss")
							});
						})
					})).then(() => { 
						apiResponse.redirect(state.doneUrl); 
					}, (error) => { 
						apiResponse.send(error); 
					});
				});
		});
	});

	function sendEmail (client, to, from, message) {
		let email = [
			`To: "${to.name}" <${to.email}>`,
			'Content-type: text/html;charset=iso-8859-1',
			'MIME-Version: 1.0',
			'Subject: My Concerns',
			'',
			`${message}`
		].join('\r\n').trim();

		let base64EncodedEmail = new Buffer(email).toString('base64');
		base64EncodedEmail = base64EncodedEmail.replace(/\+/g, '-').replace(/\//g, '_');

		return new Promise((resolve, reject) => {
			Gmail.users.messages.send({
				auth: client,
				userId: 'me',
				resource: {
					raw: base64EncodedEmail
				}
			}, (error) => {
				error ? reject(error) : resolve();
			});
		});
	}

	return route;
}
