
export default (db) => {
  return {
    loadEmailTemplate(issueSlug) {
      return db('issues').where({ issueSlug }).select('emailTemplate').then((emailTemplates) => {
        if (emailTemplates.length <= 0) {
          return Promise.reject();
        }

        return emailTemplates[0].emailTemplate;
      });
    },

    loadIssue(issueSlug) {
      return db('issues').where({ issueSlug })
        .select('issueId', 'issueSlug', 'subtext', 'description').then((issues) => {
          if (issues.length === 0) {
            return Promise.reject();
          }

          return issues[0];
        });
    }    
  };
};
